#!/bin/bash

echo "Setting Port 80 permissions for Grafana-server and restarting."
echo "Source: https://github.com/grafana/grafana/issues/1694#issuecomment-160571230" 
setcap 'cap_net_bind_service=+ep' /usr/sbin/grafana-server
systemctl restart grafana-server